@component('mail::message')
# New contact

Your receive a new contact from <strong>{{ $name }} {{ $surname }}</strong>

@component('mail::button', ['url' => url('/nova/resources/contacts')])
View contacts
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
