import { value as v, onCreated } from 'vue-function-api';
import ProductListingQuery from '@/graphql/queries/product_listing';


export default (root, variables) => {
  const listing = v({
    data: [],
    has_more_pages: false,
  });
  const page = v(1);
  const loading = v(true);

  onCreated(async () => {
    const ProductListing = ProductListingQuery(
      ({ data }) => {
        loading.value = false;
        listing.value = data.products;
      },
      {
        page: page.value,
        ...variables,
      },
      isLoading => (loading.value = isLoading),
    );

    root.$apollo.addSmartQuery('listing', ProductListing);
  });

  return {
    listing,
    page,
    loading,
  };
};
