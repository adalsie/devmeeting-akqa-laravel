import gql from 'graphql-tag';
import { get } from 'lodash';

const query = gql`query(
    $page: Int,
    $name: String,
    $description: String,
    $take: Int,
    $highlight: Boolean,
  ) {
  products(
    page: $page,
    name: $name,
    description: $description,
    take: $take,
    highlight: $highlight,
  ) {
    data {
      id,
      name,
      image,
      price,
      description,
    },
    has_more_pages,
  }
}`;

export default (resultCallback, variables, watchLoading = () => {}) => ({
  query,
  debounce: 1000,
  variables() {
    return {
      name: get(variables, 'name', ''),
      description: get(variables, 'description', ''),
      page: get(variables, 'page', ''),
      take: get(variables, 'take', ''),
      highlight: get(variables, 'highlight', ''),
    };
  },
  manual: true,
  // fetchPolicy: 'network-only',
  update: data => data.products,
  result: resultCallback,
  watchLoading,
});
