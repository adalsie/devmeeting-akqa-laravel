import Vue from 'vue';
import { plugin } from 'vue-function-api';
import VueRouter from 'vue-router';
// import VueI18n from 'vue-i18n';
// import VueApollo from 'vue-apollo';
// import VeeValidate from 'vee-validate';
// import apolloClient from './apollo';
import App from '@/views/App.vue';
// import EnLang from './lang/en';

const Home = () => import('@/views/Home.vue');
// const About = () => import('@/views/About.vue');
// const Products = () => import('@/views/Products.vue');

Vue.use(plugin);
Vue.use(VueRouter);
// Vue.use(VueI18n);
// Vue.use(VueApollo);
// Vue.use(VeeValidate);

const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      name: 'home',
      path: '/',
      component: Home,
    },
    // {
    //   name: 'about',
    //   path: '/about',
    //   component: About,
    // },
    // {
    //   name: 'products',
    //   path: '/products',
    //   component: Products,
    // },
  ],
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    }

    return { x: 0, y: 0 };
  },
});

// const messages = {
//   en: EnLang.messages,
// };

// const numberFormats = {
//   en: EnLang.numbers,
// };

// const i18n = new VueI18n({
//   locale: 'en',
//   messages,
//   numberFormats,
// });

// const apolloProvider = new VueApollo({
//   defaultClient: apolloClient,
// });

// eslint-disable-next-line no-new
new Vue({
  el: '#app',
  // i18n,
  router,
  // apolloProvider,
  render: h => h(App),
});
