const messages = {
  home: {
    hero: {
      title: 'Happiness begins here',
      description: 'Out unique feature gives you the freedom to safety get your room booked once you find the right to need exchange contact',
      ctaLabel: 'Shop now',
      ctaUrl: '/products',
    },
  },
  about: {
    side: {
      title: 'We work with create exceptional spaces',
      description: `
        Has survived not only five centuries, but also the leap into electronic typesetting, remaining the essentially unchanged.
        It was popularised in the with the release of letraset sheets containing recently.
      `,
    },
  },
};

const numbers = {
  currency: {
    style: 'currency', currency: 'USD',
  },
};

export default {
  messages,
  numbers,
};
