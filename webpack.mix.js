const mix = require('laravel-mix');
require('laravel-mix-alias');
const tailwindcss = require('tailwindcss');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const hmrOptions = {
  host: 'devmeeting-workshop.test',
  port: '8080',
};

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
  .sass('resources/sass/app.scss', 'public/css')
  .alias({
    '@': '/resources/js',
    '~': '/resources/sass',
    '@components': '/resources/js/components',
  })
  .options({
    processCssUrls: false,
    postCss: [ tailwindcss('./tailwind.config.js') ],
    hmrOptions,
  })
  .webpackConfig({
    output: {
      chunkFilename: 'js/[name].[chunkhash].js',
    },
    plugins: [
      new CleanWebpackPlugin(['public/js/*'], {verbose: false}),
    ],
  });
