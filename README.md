# Devmeeting Workshop Laravel

### Requirements

- Node
- PHP
- Mysql
- Composer
- Homebrew

#### Steps

Homebrew packages

```vim
# ~/.bashrc
export PATH="$HOME/.composer/vendor/bin:/usr/local/opt/curl/bin:$PATH"
```

```shell
> brew install node
```

```shell
> brew install php@7.2
> brew link php@7.2 --force
```

```shell
> brew install mysql@5.7
> brew link mysql@5.7 --force
```

### Troubleshooting
```bash
> mysqld_safe --skip-grant-tables
> mysql -u root
```

Inside mysql
```
use mysql;

update user set authentication_string=PASSWORD("root") where User='root';

flush privileges;
```

Create database
```
create database devmeeting-laraval;
```

Install composer
```shell
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === 'a5c698ffe4b8e849a443b120cd5ba38043260d5c4023dbf93e1558871f1f07f58274fc6f4c93bcfd858c6bd0775cd8d1') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
mv composer-setup.php /usr/local/bin/composer
```

Install valet
```shell
composer global require laravel/valet
valet install
```
