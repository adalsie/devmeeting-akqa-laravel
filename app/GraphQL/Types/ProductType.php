<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;
use App\Product;

class ProductType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Product',
        'description' => 'Product element',
        'model' => Product::class,
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of product'
            ],
            'name' => [
                'type' => Type::string(),
                'description' => 'The name of product'
            ],
            'image' => [
                'type' => Type::string(),
                'description' => 'The image of product'
            ],
            'price' => [
                'type' => Type::float(),
                'description' => 'The price of product'
            ],
            'description' => [
                'type' => Type::string(),
                'description' => 'The description of product'
            ],
            'quantity' => [
                'type' => Type::int(),
                'description' => 'The quantity of product'
            ],
            // 'highlight' => [
            //     'type' => Type::boolean(),
            //     'description' => 'Is product highlighted'
            // ],
        ];
    }

    protected function resolveImageField($root, $args)
    {
        return $root->imageUrl;
    }
}
