<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use Closure;
use GraphQL;
use App\Product;
use Illuminate\Support\Arr;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use GraphQL\Type\Definition\ResolveInfo;
use Rebing\GraphQL\Support\SelectFields;

class ProductsQuery extends Query
{
    const DEFAULT_PAGINATE_NUMBER = 12;

    protected $attributes = [
        'name' => 'products',
        'description' => 'Query list of product type',
    ];

    public function type(): Type
    {
        return GraphQL::paginate('product');
    }

    public function args(): array
    {
        return [
            // 'name' => ['name' => 'name', 'type' => Type::string()],
            // 'description' => ['name' => 'description', 'type' => Type::string()],
            // 'highlight' => ['name' => 'highlight', 'type' => Type::boolean()],
            'take' => ['name' => 'take', 'type' => Type::int()],
            'page' => ['name' => 'page', 'type' => Type::int()],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        /** @var SelectFields $fields */
        $fields = $getSelectFields();
        $select = $fields->getSelect();
        $with = $fields->getRelations();
        $page = Arr::get($args, 'page', 1);
        $perPage = Arr::get($args, 'take', self::DEFAULT_PAGINATE_NUMBER);

        $query = Product::with($with)
            ->select($select);

        // $query->where(function($query) use ($args) {
        //     if (isset($args['name'])) {
        //         $query->orWhere('name' , 'LIKE', $args['name']);
        //     }

        //     if (isset($args['description'])) {
        //         $query->orWhere('description' , 'LIKE', $args['description']);
        //     }
        // });

        // if (isset($args['highlight'])) {
        //     $query->where('highlight', $args['highlight']);
        // }

        // return $query->paginate($perPage, ['*'], 'page', $page);
        return $query->paginate($perPage, ['*'], 'page', $page);
    }
}
