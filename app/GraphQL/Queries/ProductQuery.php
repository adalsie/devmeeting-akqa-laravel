<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use Closure;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use Rebing\GraphQL\Support\SelectFields;
use Rebing\GraphQL\Support\Query;
use GraphQL;
use App\Product;

class ProductQuery extends Query
{
    protected $attributes = [
        'name' => 'product',
        'description' => 'Query product type',
    ];

    public function type(): Type
    {
        return GraphQL::type('product');
    }

    public function args(): array
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::int()],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        if (isset($args['id'])) {
            return Product::where('id' , $args['id'])->first();
        }

        return Product::first();
    }
}
