<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Requests\ContactStore;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ContactStore  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(ContactStore $request)
    public function store(Request $request)
    {
        dump($request->all());
        // $contact = new Contact;

        // $contact->fill(
        //     collect($request->validated())
        //         ->except(['attachment'])
        //         ->toArray()
        // );

        // if ($file = $request->file('file')) {
        //     $name = Str::start(
        //         $file->getClientOriginalName(),
        //         Str::random(30) . '_'
        //     );

        //     $file->storeAs('private', $name);

        //     $contact->attachment = $name;
        // }

        // $contact->save();

        // return response()->json([], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function show(Contact $contact)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit(Contact $contact)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contact $contact)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contact $contact)
    {
        //
    }
}
