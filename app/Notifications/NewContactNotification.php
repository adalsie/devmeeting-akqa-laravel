<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

// artisan queue:table
// artisan queue:failed-table
class NewContactNotification extends Notification implements ShouldQueue
// class NewContactNotification extends Notification
{
    use Queueable;

    protected $name;
    protected $surname;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($name, $surname)
    {
        $this->name = $name;
        $this->surname = $surname;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line('The introduction to the notification.')
            ->action('Notification Action', url('/'))
            ->line('Thank you for using our application!');


        // $params = [
        //     'name' => $this->name,
        //     'surname' => $this->surname,
        // ];

        // return (new MailMessage)
        //     ->subject(__('notifications.new_contact.subject'))
        //     ->markdown(
        //         'notifications.new_contact',
        //         $params
        //     );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
