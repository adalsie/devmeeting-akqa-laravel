<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence(),
        'image' => $faker->image(storage_path('app/public'), 820, 600, 'food', false),
        'price' => $faker->randomNumber(2),
        'description' => $faker->sentences(4, true),
        'quantity' => $faker->randomNumber(2),
        'highlight' => $faker->boolean(80),
    ];
});
