<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Contact;
use Faker\Generator as Faker;

$factory->define(Contact::class, function (Faker $faker) {
    return [
        'name' => $faker->firstName,
        'surname' => $faker->lastName,
        'email' => $faker->email,
        'message' => $faker->sentences(5, true),
        'attachment' => $faker->image(storage_path('app/private'), 820, 600, 'cats', false),
    ];
});
